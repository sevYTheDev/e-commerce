.. image:: https://img.shields.io/badge/licence-AGPL--3-blue.svg
   :target: http://www.gnu.org/licenses/agpl-3.0-standalone.html
   :alt: License: AGPL-3

========================================
Website Sale Suggested Products Category
========================================
Sort suggested products under Suggested Products Categories.

Configuration
=============
\-

Usage
=====
1. Navigate to Website > Configuration > Suggested Products Categories
2. Create a record. Give name and description which is shown in cart for the category.
3. Add eCommerce Categories. Products listed under these eCommerce Categories will be shown in cart under Suggested Products Categories.

Known issues / Roadmap
======================
- Products shown in cart under Suggested Products Categories are fetched from eCommerce Categories.
  Cleaner solution might be to directly set Products in Suggested Products Categories to cut out the confusing middle man eCommerce Category.

Credits
=======

Contributors
------------

* Miika Nissi <miika.nissi@tawasta.fi>

Maintainer
----------

.. image:: http://tawasta.fi/templates/tawastrap/images/logo.png
   :alt: Oy Tawasta OS Technologies Ltd.
   :target: http://tawasta.fi/

This module is maintained by Oy Tawasta OS Technologies Ltd.
