[![License: AGPL v3](https://img.shields.io/badge/License-AGPL%20v3-blue.svg)](https://www.gnu.org/licenses/agpl-3.0)
[![Pipeline Status](https://gitlab.com/tawasta/odoo/e-commerce/badges/14.0-dev/pipeline.svg)](https://gitlab.com/tawasta/odoo/e-commerce/-/pipelines/)

Invoicing
=========

[//]: # (addons)

Available addons
----------------
addon | version | maintainers | summary
--- | --- | --- | ---
[delivery_carrier_price_with_tax](delivery_carrier_price_with_tax/) | 14.0.1.0.0 |  | Show also price with tax on the delivery carrier
[website_sale_autocreate_company](website_sale_autocreate_company/) | 14.0.1.0.0 |  | Create company for website customer, if company name is set
[website_sale_billing_address](website_sale_billing_address/) | 14.0.1.0.0 |  | Website sale billing address
[website_sale_business_code](website_sale_business_code/) | 14.0.1.0.3 |  | Adds Finnish Business code to website checkout form
[website_sale_cart_fiscal_position](website_sale_cart_fiscal_position/) | 14.0.1.0.0 |  | Show fiscal position when confirming a sale
[website_sale_cart_lines_tax](website_sale_cart_lines_tax/) | 14.0.1.0.0 |  | eCommerce tax in cart lines
[website_sale_company_email](website_sale_company_email/) | 14.0.1.0.4 |  | Add different emails for ecommerce user and company
[website_sale_company_slider](website_sale_company_slider/) | 14.0.1.2.8 |  | Slider to checkout
[website_sale_coupon_company](website_sale_coupon_company/) | 14.0.1.0.0 |  | Website sale coupon company
[website_sale_create_user](website_sale_create_user/) | 14.0.1.0.1 |  | Website sale create user
[website_sale_customer_contact](website_sale_customer_contact/) | 14.0.1.0.0 |  | Adds customer contact as SO contact
[website_sale_default_country](website_sale_default_country/) | 14.0.1.0.0 |  | Website Sale Default Country
[website_sale_default_invoice_address](website_sale_default_invoice_address/) | 14.0.1.0.0 |  | Use predefined invoice address for partner, if set
[website_sale_default_privacies](website_sale_default_privacies/) | 14.0.1.0.0 |  | Website sale default privacy values
[website_sale_delivery_cost_with_tax](website_sale_delivery_cost_with_tax/) | 14.0.1.0.0 |  | Website sale delivery costs with tax
[website_sale_disable_pricelist_selector](website_sale_disable_pricelist_selector/) | 14.0.1.0.0 |  | If a pricelist is assigned to a partner, hide pricelist selector in shop
[website_sale_disable_qty_in_cart](website_sale_disable_qty_in_cart/) | 14.0.1.0.0 |  | Helper module to disable changing product quantity in cart
[website_sale_domicile](website_sale_domicile/) | 14.0.1.0.0 |  | Adds domicile to website checkout form
[website_sale_edicode](website_sale_edicode/) | 14.0.1.5.2 |  | Adds EDI code to website checkout form
[website_sale_extra_step_content_to_sale_order_note](website_sale_extra_step_content_to_sale_order_note/) | 14.0.1.0.0 |  | Extra Step Content to SO note
[website_sale_extra_step_info](website_sale_extra_step_info/) | 14.0.1.0.3 |  | Extra step info text: global and publica category
[website_sale_extra_step_replace_feedback_text](website_sale_extra_step_replace_feedback_text/) | 14.0.1.0.0 |  | Replace extra step feedback text to more information
[website_sale_extra_step_settings](website_sale_extra_step_settings/) | 14.0.1.0.1 |  | Allow hiding/showing items in extra step
[website_sale_force_address_step](website_sale_force_address_step/) | 14.0.1.0.1 |  | Never skip the address step. Disables 'express'-checkout
[website_sale_force_autoinvoice](website_sale_force_autoinvoice/) | 14.0.1.0.1 |  | Forces autoinvoice on quotations created from website_sale
[website_sale_hide_default_product_text](website_sale_hide_default_product_text/) | 14.0.1.0.0 |  | Hides the default '30-day money-back quarantee...' text
[website_sale_hide_delivery_orders](website_sale_hide_delivery_orders/) | 14.0.1.0.0 |  | Hide Delivery Orders Snippet
[website_sale_hide_login](website_sale_hide_login/) | 14.0.1.0.0 |  | Website Sale Hide Login Button
[website_sale_invoice_transmit_method](website_sale_invoice_transmit_method/) | 14.0.1.0.2 |  | Adds account invoice transmit method to checkout
[website_sale_invoicing_fee](website_sale_invoicing_fee/) | 14.0.1.0.2 |  | Adds Invoicing fee to Sale Order
[website_sale_maintenance_mode](website_sale_maintenance_mode/) | 14.0.1.0.0 |  | Website Sale Maintenance mode
[website_sale_membership_privacies](website_sale_membership_privacies/) | 14.0.1.0.0 |  | Website sale membership privacy values
[website_sale_payment_acquirer_filter](website_sale_payment_acquirer_filter/) | 14.0.1.0.0 |  | Allows filtering payment acquirers by partner type
[website_sale_payment_rules](website_sale_payment_rules/) | 14.0.1.0.1 |  | Website sale payment rules
[website_sale_payment_transmission](website_sale_payment_transmission/) | 14.0.1.0.0 |  | Invoice Transmit Method and Payment Terms for Website SO print
[website_sale_product_quantity_integer](website_sale_product_quantity_integer/) | 14.0.1.0.0 |  | Show product quantity in cart as integer
[website_sale_publication](website_sale_publication/) | 14.0.1.0.0 |  | Adds publication product to website product views
[website_sale_remove_confirmation_status](website_sale_remove_confirmation_status/) | 14.0.1.0.0 |  | Website sale: remove confirmation status
[website_sale_settings](website_sale_settings/) | 14.0.1.0.5 |  | Website sale toggleable settings under customize menu
[website_sale_show_product_code](website_sale_show_product_code/) | 14.0.1.0.0 |  | Show the product's internal reference on product page
[website_sale_split_name](website_sale_split_name/) | 14.0.1.0.1 |  | Website sale split name
[website_sale_stock_show](website_sale_stock_show/) | 14.0.1.0.0 |  | Always show availability on website if 'Sell regardless of inventory' is selected for product
[website_sale_suggested_products_category](website_sale_suggested_products_category/) | 14.0.1.0.1 |  | Show suggested products in cart under Suggested Products Categories
[website_sale_suggested_products_order_by](website_sale_suggested_products_order_by/) | 14.0.1.0.2 |  | Order suggested products using _order

[//]: # (end addons)
