# Translation of Odoo Server.
# This file contains the translation of the following modules:
# 	* website_sale_domicile
#
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server 14.0-20211220\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-02-24 13:36+0000\n"
"PO-Revision-Date: 2022-02-24 13:36+0000\n"
"Last-Translator: \n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: website_sale_domicile
#: model:ir.model,name:website_sale_domicile.model_res_partner
msgid "Contact"
msgstr "Kontakti"

#. module: website_sale_domicile
#: model:ir.model.fields,field_description:website_sale_domicile.field_res_partner__display_name
msgid "Display Name"
msgstr "Näyttönimi"

#. module: website_sale_domicile
#: model_terms:ir.ui.view,arch_db:website_sale_domicile.address
msgid "Domicile"
msgstr "Kotikunta"

#. module: website_sale_domicile
#: model:ir.model.fields,field_description:website_sale_domicile.field_res_partner__id
msgid "ID"
msgstr "Tunniste (ID)"

#. module: website_sale_domicile
#: model:ir.model.fields,field_description:website_sale_domicile.field_res_partner____last_update
msgid "Last Modified on"
msgstr "Viimeksi muokattu"
